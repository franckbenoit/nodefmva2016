var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/madb');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("connection à mongo établis ! +1");
});

var userSchema = mongoose.Schema({
  fname: String,
  lname: String,
  phone: String,
  email: String
});

var user = mongoose.model('User',userSchema);

// franck dis que c'est un middleware et c'est normale
router.use(function (req, res, next) {
  var header = req.header('Authorization');
  if(header && header == "formaeva4ever"){
    next();
  }else {
    res.send("Not Authorized");
  }
});

router.param('name', function (req, res, next, name) {
  console.log(name);
  next();
});

router.get('/(:name)?', function (req, res, next) {
  var obejctFind = (req.params.name) ? {lname:req.params.name} : {};

  user.find(obejctFind, function (err, resp) {
    if(err){
      return console.log(err);
    }
    if(resp == {}){
      return console.log("user not find");
    }

    //res.send(resp);
    console.log(resp);
    res.render('users',{users:resp});
  });
});

router.post('/', function (req, res, next) {

  if(req.body.constructor === Array){
    user.create(req.body, function (err,pUser) {
      if(err){
        console.log(err);
      }
      res.send(pUser);
    })

  }else {
    var monuser = new user(req.body);
    monuser.save();
    res.send(monuser);
  }
});

router.delete('/:name', function (req, res, next) {
      var eleFind = req.params.name;
      if (eleFind) {
        user.findOneAndRemove({lname: eleFind}, function (err) {
          if (err) {
            return console.log(err);
          }else
          return res.send("User deleted");
        })
      } else {
        return res.send("Param not found");
      }
    }
);



module.exports = router;
